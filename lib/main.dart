// ignore_for_file: unused_local_variable

import 'package:flutter/material.dart';
import 'package:flutter_application/Theme/theme.dart';
import 'package:flutter_application/screens/passingData/User.dart';
import 'screens/passingData/HomePage.dart';
import 'screens/passingData/UsersScreen.dart';

void main() {
  const userList = [
    User("User - 1", "Software Engineer"),
    User("User - 2", "Android Engineer"),
    User("User - 3", "IOS Engineer"),
    User("User - 4", "Electrical Engineer"),
    User("User - 5", "Mechanical Engineer")
  ];

  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: lightMode,
      home: const HomePage(),
    );
  }
}
