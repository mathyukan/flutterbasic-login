import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class NormalApiCall extends StatefulWidget {
  const NormalApiCall({Key? key}) : super(key: key);

  @override
  State<NormalApiCall> createState() => _NormalApiCallState();
}

class _NormalApiCallState extends State<NormalApiCall> {
  List<dynamic> products = [];

  Future<void> getProducts() async {
    String url = "https://fakestoreapi.com/products";
    var response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      setState(() {
        products = json.decode(response.body);
      });
    }
  }

  @override
  void initState() {
    getProducts();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text("Normal Api Call"),
      ),
      body: Container(
        height: 600,
        width: 400,
        child: ListView.builder(
          itemCount: products.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(products[index]["title"]),
              subtitle: Text(products[index]["price"].toString()),
            );
          },
        ),
      ),
    );
  }
}
