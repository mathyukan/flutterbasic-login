class ProductDataModel {
  int? id;
  String? name;
  String? categary;
  String? imageURL;
  String? oldPrice;
  String? price;

  ProductDataModel({
    this.id,
    this.name,
    this.categary,
    this.imageURL,
    this.oldPrice,
    this.price,
  });
  ProductDataModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    categary = json['categary'];
    imageURL = json['imageURL'];
    oldPrice = json['oldPrice'];
    price = json['price'];
  }
}
