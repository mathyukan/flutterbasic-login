class User {
  final String name;
  final String occupation;

  const User(this.name, this.occupation);
}
