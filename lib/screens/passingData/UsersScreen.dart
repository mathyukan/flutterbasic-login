import 'package:flutter/material.dart';

import 'UserDetailScreen.dart';
import 'User.dart';

class UsersScreen extends StatelessWidget {
  const UsersScreen({super.key, required this.users});

  final List<User> users;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Users'),
      ),
      body: ListView.builder(
        itemCount: users.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(users[index].name),
            subtitle: Text(users[index].occupation),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => UserDetailScreen(user: users[index]),
                ),
              );
            },
          );
        },
      ),
    );
  }
}
